const express = require("express")
const fs = require("fs")
const app = express()
const data = require("./users.json")

app.use(express.json())
app.use(express.urlencoded({extended:false}));
app.use(express.static("./public"))
app.set("view engine", "ejs")

const validation = (req,res,next)=>{
    const {email} = req.body;
    const isFind = data.find((row)=> row.email == email);
    if(isFind){
        res.redirect("/login")
    }else{
        next()
    }
}

app.get("/",(req,res)=>{
    res.render("login.ejs",);
})

app.get("/dashboard",(req,res)=>{
    const name = "Roy";
    res.render("dashboard.ejs",{name})
})

app.get("/home",(req,res)=>{
    res.render("home.ejs");
})

app.get("/game",(req,res)=>{
    res.render("game.ejs");
    res.redirect("/game")
})

app.get("/login",(req,res)=>{
    res.render("login.ejs");
    res.redirect("/login")
})

app.post("/dashboard",validation,(req,res)=>{
    const {email, password} = req.body;
    const account = {email, password}
    const users = [...data, account];

    fs.writeFileSync("./users.json", JSON.stringify(users));

    res.redirect("/dashboard")
})  

app.use((req,res)=>{
    res.status(400).render("error.ejs")
})

app.listen(8000, ()=>console.log(`Listening at http://localhost:${8000}`))